package com.nicolaslera.marvel_cards.presentation.hero_details

import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nhaarman.mockitokotlin2.whenever
import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import com.nicolaslera.marvel_cards.domain.model.TestHeroDetailsBuilder.aHeroDetails
import com.nicolaslera.marvel_cards.domain.model.TestHeroItemListBuilder.aHeroItem
import com.nicolaslera.marvel_cards.domain.model.TestHeroItemListBuilder.aHeroItemList
import com.nicolaslera.marvel_cards.presentation.core.MainActivity
import com.nicolaslera.marvel_cards.presentation.core.RecyclerViewMatcher
import com.nicolaslera.marvel_cards.presentation.di.appModule
import com.nicolaslera.marvel_cards.presentation.di.heroDetailsModule
import com.nicolaslera.marvel_cards.presentation.di.heroModule
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.test.KoinTest
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito

@RunWith(AndroidJUnit4::class)
class HeroDetailsFragmentTest : KoinTest {

    private lateinit var scenario: ActivityScenario<MainActivity>

    companion object {
        private val testHeroItem = aHeroItem(
            id = 1L,
            name = "test-name",
            imageUrl = "image-url",
            extension = "image-extension"
        )
        private val testHeroDetails = aHeroDetails(
            id = 1L,
            name = "test-name",
            imageUrl = "image-url",
            extension = "image-extension"
        )
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @Before
    fun before() {
        loadKoinModules(listOf(appModule, heroModule, heroDetailsModule))
    }

    @Test
    fun whenOpeningTheActivityAndPressingAHeroShouldGoToDetails() {

        mockRepository(
            Single.just(aHeroItemList(heroList = listOf(testHeroItem))),
            Single.just(testHeroDetails)
        )

        scenario = launchActivity()
        clickFirstItem()
        onView(withId(R.id.heroDetailsMainContainer)).check(matches(isDisplayed()))
    }

    @Test
    fun whenOpeningHeroDetailsShouldShowData() {
        mockRepository(
            Single.just(aHeroItemList(heroList = listOf(testHeroItem))),
            Single.just(testHeroDetails)
        )
        scenario = launchActivity()

        clickFirstItem()
        Thread.sleep(2000L)
        checkIfDataIsShown()
    }

    @Test
    fun whenGettingTheHeroesWithErrorShouldShowError() {
        mockRepositoryWithError()

        scenario = launchActivity()
        clickFirstItem()
        checkErrorShown()
    }

    private fun clickFirstItem() {
        onView(
            RecyclerViewMatcher.withRecyclerView(R.id.heroListRecyclerView)
                .atPosition(0)
        )
            .perform(click())
    }

    private fun checkIfDataIsShown() {
        onView(withId(R.id.heroDetailsMainContainer))
            .check(matches(hasDescendant(withId(R.id.heroDetailsNameTextView))))
            .check(matches(hasDescendant(withText(testHeroDetails.name))))
            .check(matches(hasDescendant(withId(R.id.heroDetailsImageView))))
    }

    private fun checkErrorShown() {
        onView(withId(R.id.heroDetailsErrorView)).check(matches(isDisplayed()))
        onView(withId(R.id.heroDetailsErrorView))
            .check(matches(hasDescendant(withText(R.string.hero_details_error))))
    }

    private fun mockRepository(
        singleList: Single<HeroItemList>,
        singleDetails: Single<HeroDetails>
    ) {
        declareMock<HeroRepository> {
            whenever(this.getHeroList(limit = 10, offset = 0)).thenReturn(singleList)
            whenever(this.getHeroDetails(heroId = 1L)).thenReturn(singleDetails)
        }
    }

    private fun mockRepositoryWithError() {
        declareMock<HeroRepository> {
            val testHeroItemList = aHeroItemList(
                heroList = listOf(
                    aHeroItem(
                        id = 1L,
                        name = "test-name",
                        imageUrl = "image-url",
                        extension = "image-extension"
                    )
                )
            )
            whenever(this.getHeroList(limit = 10, offset = 0))
                .thenReturn(Single.just(testHeroItemList))
            whenever(this.getHeroDetails(heroId = 1L))
                .thenReturn(Single.error(Throwable()))
        }
    }
}