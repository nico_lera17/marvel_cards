package com.nicolaslera.marvel_cards.presentation.hero_list

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nhaarman.mockitokotlin2.whenever
import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import com.nicolaslera.marvel_cards.domain.model.TestHeroItemListBuilder.aHeroItem
import com.nicolaslera.marvel_cards.domain.model.TestHeroItemListBuilder.aHeroItemList
import com.nicolaslera.marvel_cards.presentation.core.MainActivity
import com.nicolaslera.marvel_cards.presentation.core.RecyclerViewMatcher
import com.nicolaslera.marvel_cards.presentation.di.appModule
import com.nicolaslera.marvel_cards.presentation.di.heroDetailsModule
import com.nicolaslera.marvel_cards.presentation.di.heroModule
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.test.KoinTest
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.Mockito

@RunWith(AndroidJUnit4::class)
class HeroListFragmentTest : KoinTest {

    private lateinit var scenario: ActivityScenario<MainActivity>

    companion object {
        private val testHeroItem = aHeroItem(
            id = 1L,
            name = "test-name",
            imageUrl = "image-url",
            extension = "image-extension"
        )
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz ->
        Mockito.mock(clazz.java)
    }

    @Before
    fun before() {
        loadKoinModules(listOf(appModule, heroModule, heroDetailsModule))
    }

    @Test
    fun whenOpeningTheActivityItShouldLaunch() {
        mockRepository(Single.just(aHeroItemList()))

        scenario = launchActivity()
    }

    @Test
    fun whenOpeningTheActivityAndThereIsDataShouldShowData() {
        mockRepository(Single.just(aHeroItemList(heroList = listOf(testHeroItem))))

        scenario = launchActivity()

        checkIfDataIsShown(testHeroItem, 0)
    }

    @Test
    fun whenScrollingTheHeroesShouldShowTheCorrectData() {
        val testHeroItems = List(20) {
            aHeroItem(
                id = it.toLong(),
                name = "test-name-$it",
                imageUrl = "image-url-$it",
                extension = "image-extension-$it"
            )
        }
        mockRepository(Single.just(aHeroItemList(heroList = testHeroItems)))

        scenario = launchActivity()

        testHeroItems.forEachIndexed { position, heroItem ->
            onView(withId(R.id.heroListRecyclerView)).perform(
                RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(position)
            )

            checkIfDataIsShown(heroItem, position)
        }
    }

    @Test
    fun whenGettingTheHeroesAndMoreToComeShouldShowLoadMore() {
        val testHeroItems = List(20) {
            aHeroItem(
                id = it.toLong(),
                name = "test-name-$it",
                imageUrl = "image-url-$it",
                extension = "image-extension-$it"
            )
        }
        mockRepository(Single.just(aHeroItemList(heroList = testHeroItems, totalItemCount = 200)))

        scenario = launchActivity()

        checkLoadMoreShown()
    }

    @Test
    fun whenGettingTheHeroesWithErrorShouldShowError() {
        mockRepositoryWithError()

        scenario = launchActivity()

        checkErrorShown()
    }

    private fun checkIfDataIsShown(heroItem: HeroItem, position: Int) {
        onView(RecyclerViewMatcher.withRecyclerView(R.id.heroListRecyclerView).atPosition(position))
            .check(matches(hasDescendant(withText(heroItem.name))))
        onView(RecyclerViewMatcher.withRecyclerView(R.id.heroListRecyclerView).atPosition(position))
            .check(matches(hasDescendant(withId(R.id.heroDetailsImageView))))
    }

    private fun checkLoadMoreShown() {
        onView(withId(R.id.heroListLoadMoreButton)).check(matches(isDisplayed()))
    }

    private fun checkErrorShown() {
        onView(withId(R.id.heroListErrorView)).check(matches(isDisplayed()))
        onView(withId(R.id.heroListErrorView))
            .check(matches(hasDescendant(withText(R.string.hero_list_error))))
    }

    private fun mockRepository(single: Single<HeroItemList>) {
        declareMock<HeroRepository> {
            whenever(this.getHeroList(limit = 10, offset = 0)).thenReturn(single)
        }
    }

    private fun mockRepositoryWithError() {
        declareMock<HeroRepository> {
            whenever(this.getHeroList(limit = 10, offset = 0)).thenReturn(Single.error(Throwable()))
        }
    }
}