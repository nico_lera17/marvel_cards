package com.nicolaslera.marvel_cards.domain.model

import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails

object TestHeroDetailsBuilder {
    fun aHeroDetails(
        id: Long = 0,
        name: String = "",
        imageUrl: String = "",
        extension: String = ""
    ): HeroDetails {
        return HeroDetails(
            id = id,
            name = name,
            imageUrl = imageUrl,
            extension = extension
        )
    }
}