package com.nicolaslera.marvel_cards.datasource.hero.remote.mapper

import com.nicolaslera.marvel_cards.datasource.hero.remote.model.TestHeroListResponseModelBuilder.aHeroListResponse
import com.nicolaslera.marvel_cards.datasource.hero.remote.model.TestHeroListResponseModelBuilder.aHeroResult
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItemList
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class HeroItemListResponseMapperTest {

    private lateinit var heroItemResponseMapper: HeroItemResponseMapper

    @Before
    fun before() {
        heroItemResponseMapper = HeroItemResponseMapper()
    }

    @Test
    fun `when mapping from response to model should do it correctly`() {
        val testResponse = aHeroListResponse(
            results = List(2) {
                aHeroResult(
                    id = it.toLong(),
                    name = "hero-item-name-$it",
                    imageUrl = "hero-image-url-$it",
                    extension = "hero-image-extension-$it"
                )
            },
            offset = 10,
            totalResults = 20
        )

        val expectedModel = aHeroItemList(
            heroList = List(2) {
                aHeroItem(
                    id = it.toLong(),
                    name = "hero-item-name-$it",
                    imageUrl = "hero-image-url-$it",
                    extension = "hero-image-extension-$it"
                )
            },
            offset = 10,
            totalItemCount = 20
        )

        val mappedData = heroItemResponseMapper.toModel(testResponse)

        assertEquals(mappedData, expectedModel)
    }
}