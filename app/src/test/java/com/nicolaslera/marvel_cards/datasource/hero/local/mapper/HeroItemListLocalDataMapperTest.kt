package com.nicolaslera.marvel_cards.datasource.hero.local.mapper

import com.nicolaslera.marvel_cards.datasource.hero.local.model.TestHeroItemDataModelBuilder.aHeroItemDataModel
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItemList
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class HeroItemListLocalDataMapperTest {

    private lateinit var heroItemListMapper: HeroItemListMapper

    @Before
    fun before() {
        heroItemListMapper = HeroItemListMapper()
    }

    @Test
    fun `when mapping model to database model should map correctly`() {
        val testHeroItemList = aHeroItemList(
            heroList = List(2) {
                aHeroItem(
                    id = it.toLong(),
                    name = "test-name-$it",
                    imageUrl = "test-image-url-$it",
                    extension = "test-image-extension-$it"
                )
            }
        )

        val expectedHeroItemDataModel = List(2) {
            aHeroItemDataModel(
                id = it.toLong(),
                name = "test-name-$it",
                imageUrl = "test-image-url-$it",
                extension = "test-image-extension-$it"
            )
        }

        val mappedItem = heroItemListMapper.toDatabaseModel(testHeroItemList)

        assertEquals(mappedItem, expectedHeroItemDataModel)
    }

    @Test
    fun `when mapping database model to model should map correctly`() {
        val testHeroItemDataModel = List(2) {
            aHeroItemDataModel(
                id = it.toLong(),
                name = "test-name-$it",
                imageUrl = "test-image-url-$it",
                extension = "test-image-extension-$it"
            )
        }

        val expectedHeroItemList = aHeroItemList(
            heroList = List(2) {
                aHeroItem(
                    id = it.toLong(),
                    name = "test-name-$it",
                    imageUrl = "test-image-url-$it",
                    extension = "test-image-extension-$it"
                )
            },
            totalItemCount = -1
        )

        val mappedItem = heroItemListMapper.toModel(testHeroItemDataModel)

        assertEquals(mappedItem, expectedHeroItemList)
    }
}