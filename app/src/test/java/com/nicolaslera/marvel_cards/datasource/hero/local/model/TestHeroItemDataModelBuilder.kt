package com.nicolaslera.marvel_cards.datasource.hero.local.model

object TestHeroItemDataModelBuilder {
    fun aHeroItemDataModel(
        id: Long = 0L,
        name: String = "",
        imageUrl: String = "",
        extension: String = ""
    ): HeroItemDataModel {
        return HeroItemDataModel(
            uid = id,
            name = name,
            imageUrl = imageUrl,
            extension = extension
        )
    }
}