package com.nicolaslera.marvel_cards.datasource.hero.local.dao

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nicolaslera.marvel_cards.datasource.core.local.db.MarvelDatabase
import com.nicolaslera.marvel_cards.datasource.hero.local.model.HeroItemDataModel
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

@RunWith(AndroidJUnit4::class)
class HeroItemDaoTest {
    private lateinit var marvelDatabase: MarvelDatabase

    companion object {
        private val heroItemDataModelList = List(3) {
            HeroItemDataModel(
                uid = it.toLong(),
                name = "hero-name-$it",
                imageUrl = "hero-image-url-$it",
                extension = "hero-extension-$it",
            )
        }

        private const val TEST_HERO_ID = 1L
    }

    @Before
    fun before() {
        marvelDatabase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            MarvelDatabase::class.java
        ).allowMainThreadQueries().build()
    }

    @After
    fun after() {
        marvelDatabase.close()
    }

    @Test
    fun `when getting all heroes should return the list`() {
        marvelDatabase.heroItemDao().insertAll(heroItemDataModelList).blockingAwait()

        val heroes = marvelDatabase.heroItemDao().getAll().blockingGet()
        assert(heroes.isNotEmpty())
        assertEquals(heroes, heroItemDataModelList)
    }

    @Test
    fun `when getting a hero should return the hero`() {
        marvelDatabase.heroItemDao().insertAll(heroItemDataModelList).blockingAwait()

        val hero = marvelDatabase.heroItemDao().getHeroById(TEST_HERO_ID).blockingGet()
        val expectedHero = heroItemDataModelList.firstOrNull { it.uid == TEST_HERO_ID }
        assertEquals(hero, expectedHero)
    }

    @Test
    fun `when inserting the heroes should include the heroes`() {
        marvelDatabase.heroItemDao().deleteAllItems().blockingAwait()

        marvelDatabase.heroItemDao().insertAll(heroItemDataModelList).blockingAwait()

        val heroes = marvelDatabase.heroItemDao().getAll().blockingGet()
        assert(heroes.isNotEmpty())
        assertEquals(heroes, heroItemDataModelList)
    }

    @Test
    fun `when deleting the heroes should not include the heroes`() {
        marvelDatabase.heroItemDao().insertAll(heroItemDataModelList).blockingAwait()

        marvelDatabase.heroItemDao().deleteAllItems().blockingAwait()

        val heroes = marvelDatabase.heroItemDao().getAll().blockingGet()
        assert(heroes.isEmpty())
        assertNotEquals(heroes, heroItemDataModelList)
    }

}