package com.nicolaslera.marvel_cards.datasource.hero.remote.model

object TestHeroListResponseModelBuilder {
    fun aHeroListResponse(
        results: List<Result> = listOf(),
        offset: Long = 0,
        totalResults: Long = 0
    ): HeroItemListResponse {
        return HeroItemListResponse(
            code = 0,
            status = "",
            copyright = "",
            attributionText = "",
            attributionHTML = "",
            etag = "",
            data = aHeroData(
                results = results,
                offset = offset,
                totalResults = totalResults
            )
        )
    }

    private fun aHeroData(
        offset: Long = 0L,
        limit: Long = 0L,
        totalResults: Long = 0L,
        count: Long = 0L,
        results: List<Result> = listOf()
    ): Data {
        return Data(
            offset = offset,
            limit = 0,
            total = totalResults,
            count = 0,
            results = results
        )
    }

    fun aHeroResult(
        id: Long = 0L,
        name: String = "",
        imageUrl: String = "",
        extension: String = "",
        description: String = "",
        modified: String = "",
        resourceURI: String = "",
        comics: Comics = aComics(),
        series: Comics = aComics(),
        stories: Stories = aStories(),
        events: Comics = aComics(),
        urls: List<URL> = listOf()
    ): Result {
        return Result(
            id = id,
            name = name,
            description = description,
            modified = modified,
            thumbnail = aThumbnail(imageUrl, extension),
            resourceURI = resourceURI,
            comics = comics,
            series = series,
            stories = stories,
            events = events,
            urls = urls
        )
    }

    private fun aThumbnail(
        imageUrl: String = "",
        extension: String = "",
    ): Thumbnail {
        return Thumbnail(
            path = imageUrl,
            extension = extension
        )
    }

    private fun aComics(
        available: Long = 0L,
        collectionUri: String = "",
        items: List<ComicsItem> = listOf(),
        returned: Long = 0L
    ): Comics {
        return Comics(
            available = available,
            collectionURI = collectionUri,
            items = items,
            returned = returned
        )
    }

    private fun aStories(
        available: Long = 0L,
        collectionUri: String = "",
        items: List<StoriesItem> = listOf(),
        returned: Long = 0L
    ): Stories {
        return Stories(
            available = available,
            collectionURI = collectionUri,
            items = items,
            returned = returned
        )
    }
}