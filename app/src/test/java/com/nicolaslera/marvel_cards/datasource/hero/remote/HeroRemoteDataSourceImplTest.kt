package com.nicolaslera.marvel_cards.datasource.hero.remote

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.nicolaslera.marvel_cards.datasource.core.remote.service.MarvelService
import com.nicolaslera.marvel_cards.datasource.hero.remote.mapper.HeroItemResponseMapper
import com.nicolaslera.marvel_cards.datasource.hero.remote.model.TestHeroListResponseModelBuilder.aHeroListResponse
import com.nicolaslera.marvel_cards.datasource.hero.remote.model.TestHeroListResponseModelBuilder.aHeroResult
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItemList
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class HeroRemoteDataSourceImplTest {

    private val mockedMarvelService = mock<MarvelService>()
    private val mockedHeroListResponseMapper = mock<HeroItemResponseMapper>()

    private lateinit var heroRemoteDataSourceImpl: HeroRemoteDataSourceImpl

    companion object {
        private const val TEST_LIMIT = 10
        private const val TEST_OFFSET = 20
        private const val TEST_TOTAL_RESULTS = 5
    }

    private val testHeroItemListResponse = aHeroListResponse(
        results = List(3) {
            aHeroResult(
                id = it.toLong(),
                name = "hero-name-$it",
                imageUrl = "hero-image-url-$it",
                extension = "hero-image-extension-$it"
            )
        },
        offset = TEST_OFFSET.toLong(),
        totalResults = TEST_TOTAL_RESULTS.toLong()
    )

    private val expectedHeroItemList = aHeroItemList(
        heroList = List(3) {
            aHeroItem(
                id = it.toLong(),
                name = "hero-name-$it",
                imageUrl = "hero-image-url-$it",
                extension = "hero-image-extension-$it"
            )
        },
        offset = TEST_OFFSET,
        totalItemCount = TEST_TOTAL_RESULTS
    )

    @Before
    fun before() {
        heroRemoteDataSourceImpl = HeroRemoteDataSourceImpl(
            mockedMarvelService,
            mockedHeroListResponseMapper
        )
    }

    @Test
    fun `when requesting heroes should return a list`() {
        whenever(mockedMarvelService.getHeroList(TEST_LIMIT, TEST_OFFSET))
            .thenReturn(Single.just(testHeroItemListResponse))
        whenever(mockedHeroListResponseMapper.toModel(testHeroItemListResponse))
            .thenReturn(expectedHeroItemList)

        val result = heroRemoteDataSourceImpl.getHeroList(TEST_LIMIT, TEST_OFFSET).blockingGet()

        verify(mockedMarvelService).getHeroList(TEST_LIMIT, TEST_OFFSET)
        verify(mockedHeroListResponseMapper).toModel(testHeroItemListResponse)
        verifyNoMoreInteractions(mockedMarvelService)
        assertEquals(result, expectedHeroItemList)
    }
}