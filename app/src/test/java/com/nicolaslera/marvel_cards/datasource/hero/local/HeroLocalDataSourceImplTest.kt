package com.nicolaslera.marvel_cards.datasource.hero.local

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nhaarman.mockitokotlin2.*
import com.nicolaslera.marvel_cards.datasource.core.local.cache.SharedPreferencesHelper
import com.nicolaslera.marvel_cards.datasource.core.local.db.MarvelDatabase
import com.nicolaslera.marvel_cards.datasource.hero.local.dao.HeroItemDao
import com.nicolaslera.marvel_cards.datasource.hero.local.mapper.HeroDetailsMapper
import com.nicolaslera.marvel_cards.datasource.hero.local.mapper.HeroItemListMapper
import com.nicolaslera.marvel_cards.datasource.hero.local.model.HeroItemDataModel
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroDetailsBuilder.aHeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItemList
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import kotlin.test.assertEquals

@RunWith(AndroidJUnit4::class)
class HeroLocalDataSourceImplTest {

    private val mockedMarvelDatabase = mock<MarvelDatabase>()
    private val mockedHeroItemDao = mock<HeroItemDao>()
    private val mockedHeroItemListMapper = mock<HeroItemListMapper>()
    private val mockedHeroDetailsMapper = mock<HeroDetailsMapper>()
    private lateinit var preferencesHelper: SharedPreferencesHelper

    private lateinit var heroLocalDataSourceImpl: HeroLocalDataSourceImpl

    companion object {

        private const val TEST_EXPIRATION_TIME = 600000L
        private const val TEST_HERO_ID = 1L

        private val testHeroItemList = aHeroItemList(
            heroList = List(3) {
                HeroItem(
                    id = it.toLong(),
                    name = "hero-name-$it",
                    imageUrl = "hero-image-url-$it",
                    extension = "hero-extension-$it",
                )
            }
        )

        private val testHeroDetails = aHeroDetails(
            id = TEST_HERO_ID,
            name = "hero-name-1",
            imageUrl = "hero-image-url-2",
            extension = "hero-extension-3",
        )

        private val testHeroItemDataModelList = List(3) {
            HeroItemDataModel(
                uid = it.toLong(),
                name = "hero-name-$it",
                imageUrl = "hero-image-url-$it",
                extension = "hero-extension-$it",
            )
        }
    }

    @Before
    fun before() {
        preferencesHelper = SharedPreferencesHelper(ApplicationProvider.getApplicationContext())
        whenever(mockedMarvelDatabase.heroItemDao()).thenReturn(mockedHeroItemDao)

        heroLocalDataSourceImpl = HeroLocalDataSourceImpl(
            mockedMarvelDatabase,
            mockedHeroItemListMapper,
            mockedHeroDetailsMapper,
            preferencesHelper
        )
    }

    @Test
    fun `when fetching heroes with limit and offset should return that sublist`() {
        val testLimit = 1
        val testOffset = 1
        val expectedDataModelList =
            testHeroItemDataModelList.subList(testLimit, testLimit + testOffset)
        val expectedHeroItemList = testHeroItemList.copy(
            heroList = testHeroItemList.heroList.subList(testLimit, testLimit + testOffset)
        )
        whenever(mockedHeroItemDao.getAll()).thenReturn(Single.just(testHeroItemDataModelList))
        whenever(mockedHeroItemListMapper.toModel(expectedDataModelList))
            .thenReturn(expectedHeroItemList)

        val result = heroLocalDataSourceImpl.getHeroList(testLimit, testOffset).blockingGet()

        verify(mockedHeroItemDao).getAll()
        verify(mockedHeroItemListMapper).toModel(expectedDataModelList)
        verifyNoMoreInteractions(mockedHeroItemDao)
        verifyNoMoreInteractions(mockedHeroItemListMapper)
        assertEquals(result, expectedHeroItemList.copy(offset = testOffset))
    }

    @Test
    fun `when fetching a hero details should return a hero`() {
        val testDataModel = testHeroItemDataModelList[0]
        whenever(mockedHeroDetailsMapper.toModel(testDataModel))
            .thenReturn(testHeroDetails)
        whenever(mockedHeroItemDao.getHeroById(heroId = TEST_HERO_ID))
            .thenReturn(Single.just(testDataModel))

        val result = heroLocalDataSourceImpl.getHeroDetails(TEST_HERO_ID).blockingGet()

        verify(mockedHeroDetailsMapper).toModel(testDataModel)
        verify(mockedHeroItemDao).getHeroById(TEST_HERO_ID)
        verifyNoMoreInteractions(mockedHeroItemDao)
        assertEquals(result, testHeroDetails)
    }

    @Test
    fun `when setting the cache time should set the preferences time`() {
        heroLocalDataSourceImpl.setCacheTime(TEST_EXPIRATION_TIME).blockingAwait()

        assertEquals(preferencesHelper.lastCacheTime, TEST_EXPIRATION_TIME)
    }

    @Test
    fun `when checking valid cache with invalid expiration time should return false`() {
        preferencesHelper.lastCacheTime = Date().time - TEST_EXPIRATION_TIME - 10

        val result = heroLocalDataSourceImpl.isValidCache(limit = 0, offset = 0).blockingGet()

        verifyZeroInteractions(mockedHeroItemDao)
        assertEquals(result, false)
    }

    @Test
    fun `when checking valid cache without expiration and out of bounds should return false`() {
        preferencesHelper.lastCacheTime = Date().time
        whenever(mockedHeroItemDao.getAll()).thenReturn(Single.just(testHeroItemDataModelList))

        val result = heroLocalDataSourceImpl.isValidCache(limit = 3, offset = 3).blockingGet()

        verify(mockedHeroItemDao).getAll()
        verifyNoMoreInteractions(mockedHeroItemDao)
        assertEquals(result, false)
    }

    @Test
    fun `when checking valid cache without expiration and in bounds should return true`() {
        preferencesHelper.lastCacheTime = Date().time
        whenever(mockedHeroItemDao.getAll()).thenReturn(Single.just(testHeroItemDataModelList))

        val result = heroLocalDataSourceImpl.isValidCache(limit = 0, offset = 0).blockingGet()

        verify(mockedHeroItemDao).getAll()
        verifyNoMoreInteractions(mockedHeroItemDao)
        assertEquals(result, true)
    }

    @Test
    fun `when insert data database should add the items`() {
        whenever(mockedHeroItemListMapper.toDatabaseModel(testHeroItemList.heroList))
            .thenReturn(testHeroItemDataModelList)
        whenever(mockedHeroItemDao.insertAll(testHeroItemDataModelList))
            .thenReturn(Completable.complete())

        val result = heroLocalDataSourceImpl.saveHeroItems(testHeroItemList.heroList)
        verify(mockedHeroItemDao).insertAll(testHeroItemDataModelList)
        verifyNoMoreInteractions(mockedHeroItemDao)
        assertEquals(result, Completable.complete())
    }

    @Test
    fun `when clear database should delete all items`() {
        whenever(mockedHeroItemDao.deleteAllItems()).thenReturn(Completable.complete())

        val result = heroLocalDataSourceImpl.clearHeroItems()
        verify(mockedHeroItemDao).deleteAllItems()
        verifyNoMoreInteractions(mockedHeroItemDao)
        assertEquals(result, Completable.complete())
    }
}