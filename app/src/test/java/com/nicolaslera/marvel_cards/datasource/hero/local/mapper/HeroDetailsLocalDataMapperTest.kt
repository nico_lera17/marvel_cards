package com.nicolaslera.marvel_cards.datasource.hero.local.mapper

import com.nicolaslera.marvel_cards.datasource.hero.local.model.TestHeroItemDataModelBuilder.aHeroItemDataModel
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroDetailsBuilder.aHeroDetails
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class HeroDetailsLocalDataMapperTest {

    private lateinit var heroDetailsMapper: HeroDetailsMapper

    @Before
    fun before() {
        heroDetailsMapper = HeroDetailsMapper()
    }

    @Test
    fun `when mapping model to database model should map correctly`() {
        val testHeroItemList = aHeroDetails(
            id = 1L,
            name = "test-name-1",
            imageUrl = "test-image-url-1",
            extension = "test-image-extension-1"
        )

        val expectedHeroItemDataModel = aHeroItemDataModel(
            id = 1L,
            name = "test-name-1",
            imageUrl = "test-image-url-1",
            extension = "test-image-extension-1"
        )

        val mappedItem = heroDetailsMapper.toDatabaseModel(testHeroItemList)

        assertEquals(mappedItem, expectedHeroItemDataModel)
    }

    @Test
    fun `when mapping database model to model should map correctly`() {
        val testHeroItemDataModel = aHeroItemDataModel(
            id = 1L,
            name = "test-name-1",
            imageUrl = "test-image-url-1",
            extension = "test-image-extension-1"
        )

        val expectedHeroDetails = aHeroDetails(
            id = 1L,
            name = "test-name-1",
            imageUrl = "test-image-url-1",
            extension = "test-image-extension-1"
        )

        val mappedItem = heroDetailsMapper.toModel(testHeroItemDataModel)

        assertEquals(mappedItem, expectedHeroDetails)
    }
}