package com.nicolaslera.marvel_cards.presentation.hero_list.view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.*
import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItemList
import com.nicolaslera.marvel_cards.domain.hero.use_case.GetHeroesUseCase
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.core.error_handling.ErrorHandler
import com.nicolaslera.marvel_cards.presentation.core.model.DataState
import com.nicolaslera.marvel_cards.presentation.hero_list.viewmodel.HeroListViewModel
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HeroListViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()
    private val mockedGetHeroesUseCase = mock<GetHeroesUseCase>()
    private val mockedErrorHandler = mock<ErrorHandler>()

    private lateinit var heroListViewModel: HeroListViewModel

    companion object {
        private const val TEST_HERO_ID = 123L
        private const val TEST_ERROR_MESSAGE_ID = R.string.hero_list_error
        private const val TEST_LIMIT = HeroListViewModel.HERO_LIMIT
        private const val TEST_OFFSET = 0
    }

    @Before
    fun before() {
        heroListViewModel = HeroListViewModel(mockedGetHeroesUseCase, mockedErrorHandler)
    }

    @Test
    fun `when fetching correct data should load and then success`() {
        val testHeroList = aHeroItemList(heroList = listOf(aHeroItem(id = TEST_HERO_ID)))
        whenever(mockedGetHeroesUseCase.execute(Pair(TEST_LIMIT, TEST_OFFSET)))
            .thenReturn(Single.just(testHeroList))
        heroListViewModel.fetchHeroes()

        verify(mockedGetHeroesUseCase).execute(Pair(TEST_LIMIT, TEST_OFFSET))
        verify(mockedErrorHandler, never()).getErrorMessage(AppError.GET_HERO_LIST_ERROR)
        assertIsDataExpectedSuccess(testHeroList)
    }

    @Test
    fun `when fetching correct data again should load with new offset and then success`() {
        val testHeroList = aHeroItemList(heroList = listOf(aHeroItem(id = TEST_HERO_ID)))
        whenever(mockedGetHeroesUseCase.execute(Pair(TEST_LIMIT, TEST_OFFSET)))
            .thenReturn(Single.just(testHeroList))
        whenever(mockedGetHeroesUseCase.execute(Pair(TEST_LIMIT, TEST_LIMIT + TEST_OFFSET)))
            .thenReturn(Single.just(testHeroList))
        heroListViewModel.fetchHeroes()
        clearInvocations(mockedGetHeroesUseCase)
        clearInvocations(mockedErrorHandler)
        heroListViewModel.fetchHeroes()

        verify(mockedGetHeroesUseCase).execute(Pair(TEST_LIMIT, TEST_OFFSET + TEST_LIMIT))
        verify(mockedErrorHandler, never()).getErrorMessage(AppError.GET_HERO_LIST_ERROR)
        assertIsDataExpectedSuccess(testHeroList)
    }

    @Test
    fun `when fetching wrong data should load and then error`() {
        whenever(mockedGetHeroesUseCase.execute(Pair(TEST_LIMIT, TEST_OFFSET)))
            .thenReturn(Single.error(Throwable()))
        whenever(mockedErrorHandler.getErrorMessage(AppError.GET_HERO_LIST_ERROR))
            .thenReturn(TEST_ERROR_MESSAGE_ID)
        heroListViewModel.fetchHeroes()

        verify(mockedGetHeroesUseCase).execute(Pair(TEST_LIMIT, TEST_OFFSET))
        verify(mockedErrorHandler).getErrorMessage(AppError.GET_HERO_LIST_ERROR)
        assertIsDataExpectedError()
    }

    private fun assertIsDataExpectedSuccess(heroItemList: HeroItemList) {
        val data = heroListViewModel.getHeroes().value ?: DataState.Loading()
        assert(data == DataState.Success(heroItemList))
    }

    private fun assertIsDataExpectedError() {
        val error = DataState.Error<HeroDetails>(TEST_ERROR_MESSAGE_ID)
        val data = heroListViewModel.getHeroes().value ?: DataState.Loading()
        assert(data is DataState.Error)
        assert(data == error)
    }
}