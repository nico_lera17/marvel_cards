package com.nicolaslera.marvel_cards.presentation.hero_details.view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroDetailsBuilder.aHeroDetails
import com.nicolaslera.marvel_cards.domain.hero.use_case.GetHeroDetailsUseCase
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.core.error_handling.ErrorHandler
import com.nicolaslera.marvel_cards.presentation.core.model.DataState
import com.nicolaslera.marvel_cards.presentation.hero_details.viewmodel.HeroDetailsViewModel
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong

class HeroDetailsViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()
    private val mockedGetHeroDetailsUseCase = mock<GetHeroDetailsUseCase>()
    private val mockedErrorHandler = mock<ErrorHandler>()

    private lateinit var heroDetailsViewModel: HeroDetailsViewModel

    companion object {
        private const val TEST_HERO_ID = 123L
        private const val TEST_ERROR_MESSAGE_ID = R.string.hero_details_error
    }

    @Before
    fun before() {
        heroDetailsViewModel = HeroDetailsViewModel(mockedGetHeroDetailsUseCase, mockedErrorHandler)
    }

    @Test
    fun `when fetching correct data should load and then success`() {
        val testHeroDetails = aHeroDetails(id = TEST_HERO_ID)
        whenever(mockedGetHeroDetailsUseCase.execute(anyLong()))
            .thenReturn(Single.just(testHeroDetails))
        heroDetailsViewModel.fetchHeroDetails(TEST_HERO_ID)

        verify(mockedGetHeroDetailsUseCase).execute(TEST_HERO_ID)
        assertIsDataExpectedSuccess(testHeroDetails)
    }

    @Test
    fun `when fetching wrong data should load and then error`() {
        whenever(mockedGetHeroDetailsUseCase.execute(anyLong()))
            .thenReturn(Single.error(Throwable()))
        whenever(mockedErrorHandler.getErrorMessage(AppError.GET_HERO_DETAILS_ERROR))
            .thenReturn(TEST_ERROR_MESSAGE_ID)
        heroDetailsViewModel.fetchHeroDetails(TEST_HERO_ID)

        verify(mockedGetHeroDetailsUseCase).execute(TEST_HERO_ID)
        verify(mockedErrorHandler).getErrorMessage(AppError.GET_HERO_DETAILS_ERROR)
        assertIsDataExpectedError()
    }

    private fun assertIsDataExpectedSuccess(heroDetails: HeroDetails) {
        val data = heroDetailsViewModel.getHeroDetails().value ?: DataState.Loading()
        assert(data == DataState.Success(heroDetails))
    }

    private fun assertIsDataExpectedError() {
        val error = DataState.Error<HeroDetails>(TEST_ERROR_MESSAGE_ID)
        val data = heroDetailsViewModel.getHeroDetails().value ?: DataState.Loading()
        assert(data is DataState.Error)
        assert(data == error)
    }
}