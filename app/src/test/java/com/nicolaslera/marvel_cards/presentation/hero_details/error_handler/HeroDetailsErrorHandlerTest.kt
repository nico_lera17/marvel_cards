package com.nicolaslera.marvel_cards.presentation.hero_details.error_handler

import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.hero_details.error_handling.HeroDetailsErrorHandler
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class HeroDetailsErrorHandlerTest {

    private lateinit var heroDetailsErrorHandlerTest: HeroDetailsErrorHandler

    @Before
    fun before() {
        heroDetailsErrorHandlerTest = HeroDetailsErrorHandler()
    }

    @Test
    fun `when receiving the hero details error code should show hero details error message`() {
        val errorMessageId =
            heroDetailsErrorHandlerTest.getErrorMessage(AppError.GET_HERO_DETAILS_ERROR)
        assertEquals(errorMessageId, R.string.hero_details_error)
    }

    @Test
    fun `when receiving other error code should show default error message`() {
        val errorMessageId =
            heroDetailsErrorHandlerTest.getErrorMessage(AppError.GET_HERO_LIST_ERROR)
        assertEquals(errorMessageId, R.string.common_error)
    }
}