package com.nicolaslera.marvel_cards.presentation.hero_list.error_handler

import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.hero_list.error_handling.HeroListErrorHandler
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class HeroListErrorHandlerTest {

    private lateinit var heroListErrorHandler: HeroListErrorHandler

    @Before
    fun before() {
        heroListErrorHandler = HeroListErrorHandler()
    }

    @Test
    fun `when receiving the hero list error code should show hero list error message`() {
        val errorMessageId = heroListErrorHandler.getErrorMessage(AppError.GET_HERO_LIST_ERROR)
        assertEquals(errorMessageId, R.string.hero_list_error)
    }

    @Test
    fun `when receiving other error code should show default error message`() {
        val errorMessageId = heroListErrorHandler.getErrorMessage(AppError.GET_HERO_DETAILS_ERROR)
        assertEquals(errorMessageId, R.string.common_error)
    }
}