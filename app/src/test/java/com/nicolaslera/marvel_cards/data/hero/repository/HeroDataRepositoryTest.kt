package com.nicolaslera.marvel_cards.data.hero.repository

import com.nhaarman.mockitokotlin2.whenever
import com.nicolaslera.marvel_cards.data.hero.source.local.HeroLocalDataSource
import com.nicolaslera.marvel_cards.data.hero.source.remote.HeroRemoteDataSource
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItemList
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class HeroDataRepositoryTest {

    private val mockedLocalDataSource = mock(HeroLocalDataSource::class.java)
    private val mockedRemoteDataSource = mock(HeroRemoteDataSource::class.java)

    lateinit var heroRepository: HeroDataRepository

    companion object {
        private const val TEST_HERO_ID = 1234L
        private const val TEST_HERO_LIMIT = 10
        private const val TEST_HERO_OFFSET = 20
    }

    @Before
    fun before() {
        heroRepository = HeroDataRepository(mockedLocalDataSource, mockedRemoteDataSource)
    }

    @Test
    fun `when getting the hero list and is valid cache should get the list from local`() {
        whenever(mockedLocalDataSource.isValidCache(anyInt(), anyInt()))
            .thenReturn(Single.just(true))
        whenever(mockedLocalDataSource.getHeroList(anyInt(), anyInt()))
            .thenReturn(Single.just(aHeroItemList(heroList = listOf(aHeroItem()))))

        heroRepository.getHeroList(TEST_HERO_LIMIT, TEST_HERO_OFFSET).subscribe()

        verify(mockedLocalDataSource).isValidCache(TEST_HERO_LIMIT, TEST_HERO_OFFSET)
        verify(mockedLocalDataSource).getHeroList(TEST_HERO_LIMIT, TEST_HERO_OFFSET)
        verifyNoMoreInteractions(mockedLocalDataSource)
        verifyZeroInteractions(mockedRemoteDataSource)
    }

    @Test
    fun `when get hero list and no cache should clear and retrieve from remote`() {
        val testHeroList = aHeroItemList(heroList = listOf(aHeroItem()))
        stubLocalDataSourceWhenRetrievingFromRemote(testHeroList.heroList)
        whenever(mockedRemoteDataSource.getHeroList(anyInt(), anyInt()))
            .thenReturn(Single.just(testHeroList))

        heroRepository.getHeroList(TEST_HERO_LIMIT, offset = 0).subscribe()

        verify(mockedLocalDataSource).isValidCache(TEST_HERO_LIMIT, offset = 0)
        verify(mockedLocalDataSource).clearHeroItems()
        verify(mockedLocalDataSource).setCacheTime(anyLong())
        verify(mockedLocalDataSource).saveHeroItems(testHeroList.heroList)
        verify(mockedRemoteDataSource).getHeroList(TEST_HERO_LIMIT, offset = 0)
        verifyNoMoreInteractions(mockedLocalDataSource)
        verifyNoMoreInteractions(mockedRemoteDataSource)
    }

    @Test
    fun `when get more hero list and no cache should not clear and retrieve from remote`() {
        val testHeroList = aHeroItemList(heroList = listOf(aHeroItem()))
        stubLocalDataSourceWhenRetrievingFromRemote(testHeroList.heroList)
        whenever(mockedRemoteDataSource.getHeroList(anyInt(), anyInt()))
            .thenReturn(Single.just(testHeroList))

        heroRepository.getHeroList(TEST_HERO_LIMIT, TEST_HERO_OFFSET).subscribe()

        verify(mockedLocalDataSource).isValidCache(TEST_HERO_LIMIT, TEST_HERO_OFFSET)
        verify(mockedLocalDataSource).saveHeroItems(testHeroList.heroList)
        verify(mockedRemoteDataSource).getHeroList(TEST_HERO_LIMIT, TEST_HERO_OFFSET)
        verifyNoMoreInteractions(mockedLocalDataSource)
        verifyNoMoreInteractions(mockedRemoteDataSource)
    }

    private fun stubLocalDataSourceWhenRetrievingFromRemote(heroItemList: List<HeroItem>) {
        whenever(mockedLocalDataSource.isValidCache(anyInt(), anyInt()))
            .thenReturn(Single.just(false))
        whenever(mockedLocalDataSource.clearHeroItems()).thenReturn(Completable.complete())
        whenever(mockedLocalDataSource.setCacheTime(anyLong())).thenReturn(Completable.complete())
        whenever(mockedLocalDataSource.saveHeroItems(heroItems = heroItemList))
            .thenReturn(Completable.complete())
    }

    @Test
    fun `when getting a hero's details should retrieve it from local datasource`() {
        heroRepository.getHeroDetails(TEST_HERO_ID)

        verify(mockedLocalDataSource).getHeroDetails(TEST_HERO_ID)
        verifyNoMoreInteractions(mockedLocalDataSource)
        verifyZeroInteractions(mockedRemoteDataSource)
    }

}