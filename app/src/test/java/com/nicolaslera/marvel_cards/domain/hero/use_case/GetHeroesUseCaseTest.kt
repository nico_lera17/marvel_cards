package com.nicolaslera.marvel_cards.domain.hero.use_case

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import com.nicolaslera.marvel_cards.presentation.core.execution.thread.PostExecutionThread
import org.junit.Before
import org.junit.Test
import java.util.concurrent.Executor

class GetHeroesUseCaseTest {

    private val mockedHeroRepository = mock<HeroRepository>()
    private val mockedExecutor = mock<Executor>()
    private val mockedPostExecutionThread = mock<PostExecutionThread>()

    private lateinit var getHeroesUseCase: GetHeroesUseCase

    companion object {
        private const val TEST_HERO_LIMIT = 10
        private const val TEST_HERO_OFFSET = 20
    }

    @Before
    fun before() {
        getHeroesUseCase = GetHeroesUseCase(
            mockedExecutor,
            mockedPostExecutionThread,
            mockedHeroRepository
        )
    }

    @Test
    fun `when executing the use case should call the repository for the heroes`() {
        getHeroesUseCase.buildUseCase(Pair(TEST_HERO_LIMIT, TEST_HERO_OFFSET))

        verify(mockedHeroRepository).getHeroList(TEST_HERO_LIMIT, TEST_HERO_OFFSET)
    }

}