package com.nicolaslera.marvel_cards.domain.hero.model

import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroItemListBuilder.aHeroItem
import org.junit.Before
import org.junit.Test

class HeroItemTest {
    lateinit var testHeroItem: HeroItem

    companion object {
        private const val TEST_IMAGE_URL = "hero-1-image-url"
        private const val TEST_IMAGE_EXTENSION = "jpg"
        private const val TEST_PHOTO = "$TEST_IMAGE_URL/portrait_uncanny.$TEST_IMAGE_EXTENSION"
    }

    @Before
    fun before() {
        testHeroItem = aHeroItem(
            id = 1,
            name = "hero-1-name",
            imageUrl = TEST_IMAGE_URL,
            extension = TEST_IMAGE_EXTENSION
        )
    }

    @Test
    fun `when getting a photo should return the correct url`() {
        val photo = testHeroItem.getPhoto()
        assert(photo == TEST_PHOTO)
    }
}