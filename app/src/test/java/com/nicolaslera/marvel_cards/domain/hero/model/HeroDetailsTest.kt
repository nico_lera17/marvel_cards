package com.nicolaslera.marvel_cards.domain.hero.model

import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroDetailsBuilder
import org.junit.Before
import org.junit.Test

class HeroDetailsTest {
    lateinit var testHeroDetails: HeroDetails

    companion object {
        private const val TEST_IMAGE_URL = "hero-1-image-url"
        private const val TEST_IMAGE_EXTENSION = "jpg"
        private const val TEST_PHOTO = "$TEST_IMAGE_URL/portrait_uncanny.$TEST_IMAGE_EXTENSION"
    }

    @Before
    fun before() {
        testHeroDetails = TestHeroDetailsBuilder.aHeroDetails(
            id = 1,
            name = "hero-1-name",
            imageUrl = TEST_IMAGE_URL,
            extension = TEST_IMAGE_EXTENSION
        )
    }

    @Test
    fun `when getting a photo should return the correct url`() {
        val photo = testHeroDetails.getPhoto()
        assert(photo == TEST_PHOTO)
    }
}