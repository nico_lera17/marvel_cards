package com.nicolaslera.marvel_cards.domain.hero.model.builder

import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList

object TestHeroItemListBuilder {
    fun aHeroItemList(
        heroList: List<HeroItem> = listOf(),
        offset: Int = 0,
        totalItemCount: Int = 0
    ): HeroItemList {
        return HeroItemList(
            heroList = heroList,
            offset = offset,
            totalItemCount = totalItemCount
        )
    }

    fun aHeroItem(
        id: Long = 0,
        name: String = "",
        imageUrl: String = "",
        extension: String = ""
    ): HeroItem {
        return HeroItem(
            id = id,
            name = name,
            imageUrl = imageUrl,
            extension = extension
        )
    }
}