package com.nicolaslera.marvel_cards.domain.hero.use_case

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.nicolaslera.marvel_cards.domain.hero.model.builder.TestHeroDetailsBuilder.aHeroDetails
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import com.nicolaslera.marvel_cards.presentation.core.execution.thread.PostExecutionThread
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import java.util.concurrent.Executor

class GetHeroDetailsUseCaseTest {

    private val mockedHeroRepository = mock<HeroRepository>()
    private val mockedExecutor = mock<Executor>()
    private val mockedPostExecutionThread = mock<PostExecutionThread>()

    private lateinit var getHeroDetailsUseCase: GetHeroDetailsUseCase

    companion object {
        private const val TEST_HERO_ID = 123L
    }

    @Before
    fun before() {
        getHeroDetailsUseCase = GetHeroDetailsUseCase(
            mockedExecutor,
            mockedPostExecutionThread,
            mockedHeroRepository
        )
    }

    @Test
    fun `when executing the use case should call the repository for the heroes`() {
        val testHeroDetails = aHeroDetails()
        whenever(mockedHeroRepository.getHeroDetails(TEST_HERO_ID))
            .thenReturn(Single.just(testHeroDetails))
        getHeroDetailsUseCase.buildUseCase(TEST_HERO_ID)

        verify(mockedHeroRepository).getHeroDetails(TEST_HERO_ID)
    }

}