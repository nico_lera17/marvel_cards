package com.nicolaslera.marvel_cards.datasource.core.local.cache

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesHelper(context: Context) {

    companion object {
        private const val PREF_MAP_PACKAGE_NAME = "com.meep.coding.test.preferences"
        private const val PREF_KEY_LAST_CHANGE = "last_cache"
    }

    private val mapPref: SharedPreferences =
        context.getSharedPreferences(PREF_MAP_PACKAGE_NAME, Context.MODE_PRIVATE)

    var lastCacheTime: Long
        get() = mapPref.getLong(PREF_KEY_LAST_CHANGE, 0)
        set(value) = mapPref.edit().putLong(PREF_KEY_LAST_CHANGE, value).apply()

}