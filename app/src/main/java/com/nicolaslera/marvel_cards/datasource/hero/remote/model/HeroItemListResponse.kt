package com.nicolaslera.marvel_cards.datasource.hero.remote.model

data class HeroItemListResponse(
    val code: Long,
    val status: String,
    val copyright: String,
    val attributionText: String,
    val attributionHTML: String,
    val etag: String,
    val data: Data
)