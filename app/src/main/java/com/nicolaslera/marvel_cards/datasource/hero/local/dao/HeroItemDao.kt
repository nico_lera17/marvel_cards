package com.nicolaslera.marvel_cards.datasource.hero.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nicolaslera.marvel_cards.datasource.hero.local.dao.constants.HeroItemConstants
import com.nicolaslera.marvel_cards.datasource.hero.local.dao.constants.HeroItemConstants.DELETE_HERO_ITEMS
import com.nicolaslera.marvel_cards.datasource.hero.local.model.HeroItemDataModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
abstract class HeroItemDao {
    @Query(HeroItemConstants.HERO_ITEM_QUERY_ORDERED)
    abstract fun getAll(): Single<List<HeroItemDataModel>>

    @Query("${HeroItemConstants.HERO_ITEM_QUERY_BY_ID} = :heroId")
    abstract fun getHeroById(heroId: Long): Single<HeroItemDataModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(heroes: List<HeroItemDataModel>): Completable

    @Query(DELETE_HERO_ITEMS)
    abstract fun deleteAllItems(): Completable
}