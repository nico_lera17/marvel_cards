package com.nicolaslera.marvel_cards.datasource.hero.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nicolaslera.marvel_cards.datasource.hero.local.dao.constants.HeroItemConstants

@Entity(tableName = HeroItemConstants.HERO_ITEM_TABLE_NAME)
data class HeroItemDataModel(
    @PrimaryKey
    val uid: Long,
    val name: String,
    val imageUrl: String,
    val extension: String
)