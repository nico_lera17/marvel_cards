package com.nicolaslera.marvel_cards.datasource.hero.remote

import com.nicolaslera.marvel_cards.data.hero.source.remote.HeroRemoteDataSource
import com.nicolaslera.marvel_cards.datasource.core.remote.service.MarvelService
import com.nicolaslera.marvel_cards.datasource.hero.remote.mapper.HeroItemResponseMapper
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import io.reactivex.rxjava3.core.Single

class HeroRemoteDataSourceImpl(
    private val marvelService: MarvelService,
    private val heroItemResponseMapper: HeroItemResponseMapper
) : HeroRemoteDataSource {
    override fun getHeroList(limit: Int, offset: Int): Single<HeroItemList> {
        return marvelService.getHeroList(limit, offset).map(heroItemResponseMapper::toModel)
    }
}