package com.nicolaslera.marvel_cards.datasource.core.remote.service.interceptor

import com.nicolaslera.marvel_cards.datasource.core.remote.service.utils.ServiceUtils
import okhttp3.Interceptor
import okhttp3.Response
import java.util.*

class QueryInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalUrl = chain.request().url

        val ts = Date().time
        val hash = ServiceUtils.generateHash(ts)

        val url = originalUrl.newBuilder()
            .addQueryParameter("apikey", ServiceUtils.getPublicKey())
            .addQueryParameter("ts", ts.toString())
            .addQueryParameter("hash", hash)
            .build()

        val request = originalRequest.newBuilder().url(url).build()

        return chain.proceed(request)
    }
}