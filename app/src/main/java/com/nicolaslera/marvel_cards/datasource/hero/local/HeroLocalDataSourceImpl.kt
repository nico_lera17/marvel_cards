package com.nicolaslera.marvel_cards.datasource.hero.local

import com.nicolaslera.marvel_cards.data.hero.source.local.HeroLocalDataSource
import com.nicolaslera.marvel_cards.datasource.core.local.cache.SharedPreferencesHelper
import com.nicolaslera.marvel_cards.datasource.core.local.db.MarvelDatabase
import com.nicolaslera.marvel_cards.datasource.hero.local.mapper.HeroDetailsMapper
import com.nicolaslera.marvel_cards.datasource.hero.local.mapper.HeroItemListMapper
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

class HeroLocalDataSourceImpl(
    private val marvelDatabase: MarvelDatabase,
    private val heroItemLocalListMapper: HeroItemListMapper,
    private val heroDetailsMapper: HeroDetailsMapper,
    private val preferencesHelper: SharedPreferencesHelper
) : HeroLocalDataSource {

    companion object {
        private const val EXPIRATION_TIME = (60 * 10 * 1000).toLong() // 10 minute
    }

    override fun getHeroList(limit: Int, offset: Int): Single<HeroItemList> {
        return marvelDatabase.heroItemDao().getAll().map {
            val expectedEnd = offset + limit
            val endOfCrop = if (expectedEnd > it.size) it.size else expectedEnd
            val croppedList = it.subList(offset, endOfCrop)
            heroItemLocalListMapper.toModel(croppedList).copy(offset = offset)
        }
    }

    override fun getHeroDetails(heroId: Long): Single<HeroDetails> {
        return marvelDatabase.heroItemDao().getHeroById(heroId).map(heroDetailsMapper::toModel)
    }

    override fun isValidCache(limit: Int, offset: Int): Single<Boolean> {
        return Single.defer {
            val currentTime = System.currentTimeMillis()
            val lastTime = preferencesHelper.lastCacheTime
            val expired = currentTime - lastTime > EXPIRATION_TIME
            if (!expired) {
                marvelDatabase.heroItemDao().getAll().map {
                    it.isNotEmpty() && isInBounds(it.size, limit, offset)
                }
            } else {
                Single.just(false)
            }
        }
    }

    private fun isInBounds(heroItemCount: Int, limit: Int, offset: Int): Boolean {
        return heroItemCount >= (limit + offset)
    }

    override fun setCacheTime(last: Long): Completable {
        return Completable.defer {
            preferencesHelper.lastCacheTime = last
            Completable.complete()
        }
    }

    override fun saveHeroItems(heroItems: List<HeroItem>): Completable {
        return marvelDatabase.heroItemDao()
            .insertAll(heroItemLocalListMapper.toDatabaseModel(heroItems))
    }

    override fun clearHeroItems(): Completable {
        return marvelDatabase.heroItemDao().deleteAllItems()
    }


}