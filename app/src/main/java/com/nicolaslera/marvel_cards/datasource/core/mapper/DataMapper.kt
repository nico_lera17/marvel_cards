package com.nicolaslera.marvel_cards.datasource.core.mapper

interface DataMapper<A, B> {
    fun toDatabaseModel(data: A): B
    fun toModel(data: B): A
}