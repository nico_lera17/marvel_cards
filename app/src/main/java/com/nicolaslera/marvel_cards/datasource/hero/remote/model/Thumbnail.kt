package com.nicolaslera.marvel_cards.datasource.hero.remote.model

data class Thumbnail(
    val path: String,
    val extension: String
)