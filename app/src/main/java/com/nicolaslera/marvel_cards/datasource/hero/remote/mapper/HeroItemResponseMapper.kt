package com.nicolaslera.marvel_cards.datasource.hero.remote.mapper

import com.nicolaslera.marvel_cards.datasource.core.mapper.ResponseMapper
import com.nicolaslera.marvel_cards.datasource.hero.remote.model.HeroItemListResponse
import com.nicolaslera.marvel_cards.datasource.hero.remote.model.Result
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList

class HeroItemResponseMapper : ResponseMapper<HeroItemListResponse, HeroItemList> {
    override fun toModel(response: HeroItemListResponse): HeroItemList {
        return HeroItemList(
            heroList = response.data.results.map(this::toModel),
            totalItemCount = response.data.total.toInt(),
            offset = response.data.offset.toInt()
        )
    }

    private fun toModel(result: Result): HeroItem {
        return HeroItem(
            id = result.id,
            name = result.name,
            imageUrl = result.thumbnail.path,
            extension = result.thumbnail.extension.lowercase()
        )
    }
}