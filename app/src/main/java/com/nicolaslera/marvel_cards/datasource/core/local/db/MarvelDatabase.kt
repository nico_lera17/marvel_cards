package com.nicolaslera.marvel_cards.datasource.core.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nicolaslera.marvel_cards.datasource.hero.local.dao.HeroItemDao
import com.nicolaslera.marvel_cards.datasource.hero.local.model.HeroItemDataModel

@Database(entities = [HeroItemDataModel::class], version = 1)
abstract class MarvelDatabase : RoomDatabase() {
    abstract fun heroItemDao(): HeroItemDao
}