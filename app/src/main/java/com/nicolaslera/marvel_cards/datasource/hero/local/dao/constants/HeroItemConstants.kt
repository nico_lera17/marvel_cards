package com.nicolaslera.marvel_cards.datasource.hero.local.dao.constants

object HeroItemConstants {
    const val HERO_ITEM_TABLE_NAME = "HERO_ITEM"
    const val HERO_ITEM_QUERY = "SELECT * FROM $HERO_ITEM_TABLE_NAME"
    const val HERO_ITEM_QUERY_ORDERED = "$HERO_ITEM_QUERY ORDER BY name"
    const val HERO_ITEM_QUERY_BY_ID = "$HERO_ITEM_QUERY WHERE uid"
    const val DELETE_HERO_ITEMS = "DELETE FROM $HERO_ITEM_TABLE_NAME"
}