package com.nicolaslera.marvel_cards.datasource.hero.local.mapper

import com.nicolaslera.marvel_cards.datasource.core.mapper.DataMapper
import com.nicolaslera.marvel_cards.datasource.hero.local.model.HeroItemDataModel
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList

class HeroItemListMapper : DataMapper<HeroItemList, List<HeroItemDataModel>> {
    override fun toDatabaseModel(data: HeroItemList): List<HeroItemDataModel> {
        return toDatabaseModel(data.heroList)
    }

    override fun toModel(data: List<HeroItemDataModel>): HeroItemList {
        return HeroItemList(
            heroList = data.map {
                HeroItem(it.uid, it.name, it.imageUrl, it.extension)
            },
            totalItemCount = -1,
            offset = 0
        )
    }

    fun toDatabaseModel(data: List<HeroItem>): List<HeroItemDataModel> {
        return data.map {
            HeroItemDataModel(
                it.id,
                it.name,
                it.imageUrl,
                it.extension
            )
        }
    }
}