package com.nicolaslera.marvel_cards.datasource.core.remote.service.utils

import com.nicolaslera.marvel_cards.BuildConfig
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

object ServiceUtils {

    fun generateHash(ts: Long): String {
        val keys = BuildConfig.MARVEL_PRIVATE_KEY + BuildConfig.MARVEL_PUBLIC_KEY
        return md5(ts.toString() + keys)
    }

    private fun md5(stringToHash: String): String {
        val md5 = "md5"

        try {
            val digest = MessageDigest.getInstance(md5)
            digest.update(stringToHash.toByteArray())
            val messageDigest = digest.digest()

            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2) {
                    h = "0$h"
                }
                hexString.append(h)
            }
            return hexString.toString()

        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return ""
    }

    fun getPublicKey(): String {
        return BuildConfig.MARVEL_PUBLIC_KEY
    }
}