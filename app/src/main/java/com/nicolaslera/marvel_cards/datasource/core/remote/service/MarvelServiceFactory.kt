package com.nicolaslera.marvel_cards.datasource.core.remote.service

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.nicolaslera.marvel_cards.datasource.core.remote.service.interceptor.QueryInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object MarvelServiceFactory {

    fun makeMapService(): MarvelService {
        return makeMapHttpService(makeHttpClient(), makeGson())
    }

    private fun makeMapHttpService(okHttpClient: OkHttpClient, gson: Gson): MarvelService {
        val retrofit = Retrofit.Builder()
            .baseUrl(MarvelService.BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        return retrofit.create(MarvelService::class.java)
    }

    private fun makeHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(makeLoginInterceptor())
            .addInterceptor(makeQueryInterceptor())
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .build()
    }

    private fun makeGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .setDateFormat("yyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

    private fun makeLoginInterceptor(): Interceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    private fun makeQueryInterceptor(): Interceptor {
        return QueryInterceptor()
    }
}