package com.nicolaslera.marvel_cards.datasource.core.remote.service

import com.nicolaslera.marvel_cards.datasource.hero.remote.model.HeroItemListResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MarvelService {

    companion object {
        const val BASE_URL = "https://gateway.marvel.com"
    }

    @GET("/v1/public/characters")
    fun getHeroList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Single<HeroItemListResponse>
}