package com.nicolaslera.marvel_cards.datasource.core.mapper

interface ResponseMapper<A, B> {
    fun toModel(response: A): B
}