package com.nicolaslera.marvel_cards.datasource.hero.local.mapper

import com.nicolaslera.marvel_cards.datasource.core.mapper.DataMapper
import com.nicolaslera.marvel_cards.datasource.hero.local.model.HeroItemDataModel
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails

class HeroDetailsMapper : DataMapper<HeroDetails, HeroItemDataModel> {

    override fun toDatabaseModel(data: HeroDetails): HeroItemDataModel {
        return HeroItemDataModel(
            data.id,
            data.name,
            data.imageUrl,
            data.extension
        )
    }

    override fun toModel(data: HeroItemDataModel): HeroDetails {
        return HeroDetails(
            data.uid,
            data.name,
            data.imageUrl,
            data.extension
        )
    }
}