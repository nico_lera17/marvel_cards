package com.nicolaslera.marvel_cards.domain.hero.use_case

import com.nicolaslera.marvel_cards.domain.core.SingleUseCase
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import com.nicolaslera.marvel_cards.presentation.core.execution.thread.PostExecutionThread
import io.reactivex.rxjava3.core.Single
import java.util.concurrent.Executor

class GetHeroesUseCase(
    threadExecutor: Executor,
    postExecutionThread: PostExecutionThread,
    private val heroRepository: HeroRepository
) : SingleUseCase<Pair<Int, Int>, HeroItemList>(threadExecutor, postExecutionThread) {

    override fun buildUseCase(params: Pair<Int, Int>?): Single<HeroItemList> {
        checkNotNull(params)
        return heroRepository.getHeroList(params.first, params.second)
    }
}