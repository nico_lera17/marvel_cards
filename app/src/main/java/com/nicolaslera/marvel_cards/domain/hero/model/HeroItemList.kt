package com.nicolaslera.marvel_cards.domain.hero.model

data class HeroItemList(
    val heroList: List<HeroItem>,
    val totalItemCount: Int,
    val offset: Int
) {
    companion object {
        fun getEmpty() = HeroItemList(listOf(), 0, 0)
    }
}