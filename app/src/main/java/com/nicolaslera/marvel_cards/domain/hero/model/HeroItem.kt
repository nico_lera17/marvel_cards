package com.nicolaslera.marvel_cards.domain.hero.model

data class HeroItem(
    val id: Long,
    val name: String,
    val imageUrl: String,
    val extension: String
) {
    fun getPhoto() = "$imageUrl/portrait_uncanny.$extension"
}