package com.nicolaslera.marvel_cards.domain.hero.repository

import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import io.reactivex.rxjava3.core.Single

interface HeroRepository {

    fun getHeroList(limit: Int, offset: Int): Single<HeroItemList>

    fun getHeroDetails(heroId: Long): Single<HeroDetails>
}