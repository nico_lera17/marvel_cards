package com.nicolaslera.marvel_cards.domain.hero.use_case

import com.nicolaslera.marvel_cards.domain.core.SingleUseCase
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import com.nicolaslera.marvel_cards.presentation.core.execution.thread.PostExecutionThread
import io.reactivex.rxjava3.core.Single
import java.util.concurrent.Executor

open class GetHeroDetailsUseCase(
    threadExecutor: Executor,
    postExecutionThread: PostExecutionThread,
    private val heroRepository: HeroRepository
) : SingleUseCase<Long, HeroDetails>(threadExecutor, postExecutionThread) {
    override fun buildUseCase(params: Long?): Single<HeroDetails> {
        checkNotNull(params)
        return heroRepository.getHeroDetails(params)
    }
}