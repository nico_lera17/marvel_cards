package com.nicolaslera.marvel_cards.domain.core

import com.nicolaslera.marvel_cards.presentation.core.execution.thread.PostExecutionThread
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.Executor

abstract class SingleUseCase<in Params, T> constructor(
    private val threadExecutor: Executor,
    private val postExecutionThread: PostExecutionThread
) {

    abstract fun buildUseCase(params: Params? = null): Single<T>

    open fun execute(params: Params? = null): Single<T> {
        return this.buildUseCase(params)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.scheduler)
    }
}