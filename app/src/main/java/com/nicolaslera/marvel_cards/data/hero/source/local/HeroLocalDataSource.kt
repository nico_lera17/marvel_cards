package com.nicolaslera.marvel_cards.data.hero.source.local

import com.nicolaslera.marvel_cards.data.hero.source.HeroDataSource
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface HeroLocalDataSource : HeroDataSource {

    fun getHeroDetails(heroId: Long): Single<HeroDetails>

    fun isValidCache(limit: Int, offset: Int): Single<Boolean>

    fun setCacheTime(last: Long): Completable

    fun saveHeroItems(heroItems: List<HeroItem>): Completable

    fun clearHeroItems(): Completable
}