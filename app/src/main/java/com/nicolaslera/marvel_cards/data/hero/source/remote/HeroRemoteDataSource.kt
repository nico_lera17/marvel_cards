package com.nicolaslera.marvel_cards.data.hero.source.remote

import com.nicolaslera.marvel_cards.data.hero.source.HeroDataSource

interface HeroRemoteDataSource : HeroDataSource