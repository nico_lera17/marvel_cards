package com.nicolaslera.marvel_cards.data.hero.source

import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import io.reactivex.rxjava3.core.Single

interface HeroDataSource {

    fun getHeroList(limit: Int, offset: Int): Single<HeroItemList>
}