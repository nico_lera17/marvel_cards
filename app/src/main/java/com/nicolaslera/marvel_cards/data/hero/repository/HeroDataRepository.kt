package com.nicolaslera.marvel_cards.data.hero.repository

import com.nicolaslera.marvel_cards.data.hero.source.local.HeroLocalDataSource
import com.nicolaslera.marvel_cards.data.hero.source.remote.HeroRemoteDataSource
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import java.util.*

class HeroDataRepository(
    private val localDataSource: HeroLocalDataSource,
    private val remoteDataSource: HeroRemoteDataSource
) : HeroRepository {

    override fun getHeroList(limit: Int, offset: Int): Single<HeroItemList> {
        return localDataSource.isValidCache(limit, offset).flatMap { isValidCache ->
            if (isValidCache) {
                localDataSource.getHeroList(limit, offset)
            } else {
                remoteDataSource.getHeroList(limit, offset).flatMap { heroItems ->
                    clearCache(offset).andThen(
                        saveHeroItems(heroItems.heroList)
                    ).toSingle { heroItems }
                }
            }
        }
    }

    override fun getHeroDetails(heroId: Long): Single<HeroDetails> {
        return localDataSource.getHeroDetails(heroId)
    }

    private fun clearCache(offset: Int): Completable {
        return if (offset == 0) {
            val currentTimeMillis = Date().time
            localDataSource.clearHeroItems()
                .andThen(localDataSource.setCacheTime(currentTimeMillis))
        } else {
            Completable.complete()
        }
    }

    private fun saveHeroItems(heroItems: List<HeroItem>): Completable {
        return localDataSource.saveHeroItems(heroItems)
    }
}