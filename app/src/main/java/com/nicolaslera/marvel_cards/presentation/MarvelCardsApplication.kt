package com.nicolaslera.marvel_cards.presentation

import android.app.Application
import com.nicolaslera.marvel_cards.presentation.di.appModule
import com.nicolaslera.marvel_cards.presentation.di.heroDetailsModule
import com.nicolaslera.marvel_cards.presentation.di.heroModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MarvelCardsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MarvelCardsApplication)
            modules(appModule, heroModule, heroDetailsModule)
        }
    }

}