package com.nicolaslera.marvel_cards.presentation.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.nicolaslera.marvel_cards.databinding.MainActivityBinding
import com.nicolaslera.marvel_cards.presentation.hero_list.HeroListFragmentDirections

class MainActivity : AppCompatActivity() {

    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    fun navigateToHeroDetails(heroId: Long) {
        val navController = findNavController(binding.navHostFragment.id)
        val action = HeroListFragmentDirections.navigationToHeroDetailFragment(heroId)
        navController.navigate(action)
    }
}