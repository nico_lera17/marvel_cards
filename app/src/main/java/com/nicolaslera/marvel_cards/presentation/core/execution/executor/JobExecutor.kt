package com.nicolaslera.marvel_cards.presentation.core.execution.executor

import java.util.concurrent.*

class JobExecutor : Executor {

    private val workQueue: LinkedBlockingQueue<Runnable> = LinkedBlockingQueue()

    private val threadPoolExecutor: ThreadPoolExecutor

    private val threadFactory: ThreadFactory

    companion object {
        private const val INIT_POOL_SIZE = 3
        private const val MAX_POOL_SIZE = 5
        private const val KEEP_ALIVE_TIME = 10L
        private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS
    }

    init {
        this.threadFactory = Factory()
        this.threadPoolExecutor = ThreadPoolExecutor(
            INIT_POOL_SIZE,
            MAX_POOL_SIZE,
            KEEP_ALIVE_TIME,
            KEEP_ALIVE_TIME_UNIT,
            this.workQueue,
            this.threadFactory
        )
    }

    override fun execute(p0: Runnable?) {
        if (p0 == null) {
            throw IllegalArgumentException("Runnable is null")
        }

        this.threadPoolExecutor.execute(p0)
    }

    private class Factory : ThreadFactory {
        private var counter = 0

        companion object {
            private const val THREAD_NAME = "android_"
        }

        override fun newThread(p0: Runnable): Thread {
            return Thread(p0, THREAD_NAME + counter++)
        }
    }
}