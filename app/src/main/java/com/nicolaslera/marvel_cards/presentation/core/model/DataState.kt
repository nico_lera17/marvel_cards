package com.nicolaslera.marvel_cards.presentation.core.model

import androidx.annotation.StringRes

sealed class DataState<out T> {

    class Loading<T> : DataState<T>()
    data class Success<T>(val data: T) : DataState<T>()
    data class Error<T>(@StringRes val error: Int) : DataState<T>()
}
