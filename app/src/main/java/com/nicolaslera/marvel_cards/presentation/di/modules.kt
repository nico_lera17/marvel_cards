package com.nicolaslera.marvel_cards.presentation.di

import androidx.room.Room
import com.nicolaslera.marvel_cards.data.hero.repository.HeroDataRepository
import com.nicolaslera.marvel_cards.data.hero.source.local.HeroLocalDataSource
import com.nicolaslera.marvel_cards.data.hero.source.remote.HeroRemoteDataSource
import com.nicolaslera.marvel_cards.datasource.core.local.cache.SharedPreferencesHelper
import com.nicolaslera.marvel_cards.datasource.core.local.db.MarvelDatabase
import com.nicolaslera.marvel_cards.datasource.core.remote.service.MarvelServiceFactory
import com.nicolaslera.marvel_cards.datasource.hero.local.HeroLocalDataSourceImpl
import com.nicolaslera.marvel_cards.datasource.hero.local.mapper.HeroDetailsMapper
import com.nicolaslera.marvel_cards.datasource.hero.local.mapper.HeroItemListMapper
import com.nicolaslera.marvel_cards.datasource.hero.remote.HeroRemoteDataSourceImpl
import com.nicolaslera.marvel_cards.datasource.hero.remote.mapper.HeroItemResponseMapper
import com.nicolaslera.marvel_cards.domain.hero.repository.HeroRepository
import com.nicolaslera.marvel_cards.domain.hero.use_case.GetHeroDetailsUseCase
import com.nicolaslera.marvel_cards.domain.hero.use_case.GetHeroesUseCase
import com.nicolaslera.marvel_cards.presentation.core.error_handling.ErrorHandler
import com.nicolaslera.marvel_cards.presentation.core.execution.executor.JobExecutor
import com.nicolaslera.marvel_cards.presentation.core.execution.thread.PostExecutionThread
import com.nicolaslera.marvel_cards.presentation.core.execution.thread.UiThread
import com.nicolaslera.marvel_cards.presentation.hero_details.error_handling.HeroDetailsErrorHandler
import com.nicolaslera.marvel_cards.presentation.hero_details.viewmodel.HeroDetailsViewModel
import com.nicolaslera.marvel_cards.presentation.hero_list.adapter.HeroListAdapter
import com.nicolaslera.marvel_cards.presentation.hero_list.error_handling.HeroListErrorHandler
import com.nicolaslera.marvel_cards.presentation.hero_list.viewmodel.HeroListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.util.concurrent.Executor

val appModule = module {
    factory { SharedPreferencesHelper(androidContext()) }

    single {
        Room.databaseBuilder(
            androidContext(),
            MarvelDatabase::class.java, "marvel.db"
        ).build()
    }

    factory<PostExecutionThread> { UiThread() }
    factory<Executor> { JobExecutor() }

    factory { MarvelServiceFactory.makeMapService() }
}

val heroModule = module {
    // Hero presentation
    factory<ErrorHandler>(named("HeroListErrorHandler")) { HeroListErrorHandler() }
    viewModel { HeroListViewModel(get(), get(named("HeroListErrorHandler"))) }
    factory { HeroListAdapter() }

    // Use cases
    factory { GetHeroesUseCase(get(), get(), get()) }

    // Data Mappers
    factory { HeroItemListMapper() }
    factory { HeroItemResponseMapper() }

    // Hero data repository
    factory<HeroRepository> { HeroDataRepository(get(), get()) }
    factory<HeroLocalDataSource> { HeroLocalDataSourceImpl(get(), get(), get(), get()) }
    factory<HeroRemoteDataSource> { HeroRemoteDataSourceImpl(get(), get()) }
}

val heroDetailsModule = module {
    // Hero details presentation
    factory<ErrorHandler>(named("HeroDetailErrorHandler")) { HeroDetailsErrorHandler() }
    viewModel { HeroDetailsViewModel(get(), get(named("HeroDetailErrorHandler"))) }

    // Use cases
    factory { GetHeroDetailsUseCase(get(), get(), get()) }

    // Data mappers
    factory { HeroDetailsMapper() }
}