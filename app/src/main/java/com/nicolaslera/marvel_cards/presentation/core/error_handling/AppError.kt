package com.nicolaslera.marvel_cards.presentation.core.error_handling

enum class AppError constructor(val code: Long) {
    GET_HERO_LIST_ERROR(1L),
    GET_HERO_DETAILS_ERROR(2L)
}
