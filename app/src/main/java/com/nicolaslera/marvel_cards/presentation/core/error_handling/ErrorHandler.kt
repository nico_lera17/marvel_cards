package com.nicolaslera.marvel_cards.presentation.core.error_handling

import com.nicolaslera.marvel_cards.R

abstract class ErrorHandler {
    open fun getErrorMessage(code: AppError): Int {
        return R.string.common_error
    }
}