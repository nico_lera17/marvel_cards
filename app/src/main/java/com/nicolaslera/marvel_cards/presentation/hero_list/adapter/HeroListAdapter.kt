package com.nicolaslera.marvel_cards.presentation.hero_list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicolaslera.marvel_cards.databinding.HeroListItemViewBinding
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.presentation.core.view.loadFromUrl

class HeroListAdapter : RecyclerView.Adapter<HeroListAdapter.ViewHolder>() {

    private var heroList: MutableList<HeroItem> = mutableListOf()
    lateinit var onHeroSelectedListener: OnHeroSelectedListener

    fun addHeroes(heroList: List<HeroItem>) {
        this.heroList.addAll(heroList)
    }

    fun clear() {
        this.heroList = mutableListOf()
    }

    interface OnHeroSelectedListener {
        fun onHeroSelected(heroItem: HeroItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: HeroListItemViewBinding =
            HeroListItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val hero = heroList[position]
        holder.setHero(hero)
        holder.itemView.setOnClickListener { onHeroSelectedListener.onHeroSelected(hero) }
    }

    override fun getItemCount(): Int {
        return heroList.size
    }

    class ViewHolder(
        private val binding: HeroListItemViewBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun setHero(heroItem: HeroItem) {
            with(binding) {
                heroDetailsNameTextView.text = heroItem.name
                val imageUrl = heroItem.getPhoto()
                heroDetailsImageView.loadFromUrl(imageUrl, itemView.context)
            }
        }

    }
}