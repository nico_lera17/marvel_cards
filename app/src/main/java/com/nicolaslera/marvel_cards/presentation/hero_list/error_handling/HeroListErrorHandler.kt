package com.nicolaslera.marvel_cards.presentation.hero_list.error_handling

import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.core.error_handling.ErrorHandler

class HeroListErrorHandler : ErrorHandler() {
    override fun getErrorMessage(code: AppError): Int {
        return when (code) {
            AppError.GET_HERO_LIST_ERROR -> R.string.hero_list_error
            else -> R.string.common_error
        }
    }
}