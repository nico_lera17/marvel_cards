package com.nicolaslera.marvel_cards.presentation.hero_list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import com.nicolaslera.marvel_cards.domain.hero.use_case.GetHeroesUseCase
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.core.error_handling.ErrorHandler
import com.nicolaslera.marvel_cards.presentation.core.model.DataState
import com.nicolaslera.marvel_cards.presentation.core.view_model.BaseViewModel

typealias HeroListDataState = DataState<HeroItemList>

class HeroListViewModel(
    private val getHeroesUseCase: GetHeroesUseCase,
    private val errorHandler: ErrorHandler
) : BaseViewModel() {

    private val heroesLiveData: MutableLiveData<HeroListDataState> = MutableLiveData()

    companion object {
        const val HERO_LIMIT = 10
    }

    private var offset = 0

    fun getHeroes(): LiveData<HeroListDataState> = heroesLiveData

    fun fetchHeroes() {
        heroesLiveData.value = DataState.Loading()
        val params = Pair(HERO_LIMIT, offset)
        compositeDisposable.add(getHeroesUseCase.execute(params).subscribe({
            heroesLiveData.value = DataState.Success(it)
            offset += HERO_LIMIT
        }, {
            heroesLiveData.value =
                DataState.Error(errorHandler.getErrorMessage(AppError.GET_HERO_LIST_ERROR))
        }))
    }

    fun onClear() {
        offset = 0
        heroesLiveData.value = DataState.Success(HeroItemList.getEmpty())
    }
}