package com.nicolaslera.marvel_cards.presentation.hero_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.nicolaslera.marvel_cards.databinding.HeroDetailsFragmentBinding
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.presentation.core.model.DataState
import com.nicolaslera.marvel_cards.presentation.core.view.hide
import com.nicolaslera.marvel_cards.presentation.core.view.loadFromUrl
import com.nicolaslera.marvel_cards.presentation.core.view.show
import com.nicolaslera.marvel_cards.presentation.hero_details.viewmodel.HeroDetailsDataState
import com.nicolaslera.marvel_cards.presentation.hero_details.viewmodel.HeroDetailsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HeroDetailsFragment : Fragment() {

    private val heroDetailsViewModel: HeroDetailsViewModel by viewModel()
    private var _binding: HeroDetailsFragmentBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<HeroDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HeroDetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupData()
    }

    private fun setupView() {
        binding.heroDetailsErrorView.errorRetry.setOnClickListener {
            heroDetailsViewModel.fetchHeroDetails(args.heroId)
        }
    }

    private fun setupData() {
        heroDetailsViewModel.getHeroDetails().observe(viewLifecycleOwner, {
            handleDataState(it)
        })

        if (heroDetailsViewModel.getHeroDetails().value == null) {
            heroDetailsViewModel.fetchHeroDetails(args.heroId)
        }
    }

    private fun handleDataState(heroDetailsDataState: HeroDetailsDataState) {
        when (heroDetailsDataState) {
            is DataState.Loading -> showLoadingView()
            is DataState.Success -> handleData(heroDetailsDataState.data)
            is DataState.Error -> showErrorView(heroDetailsDataState.error)
        }
    }

    private fun handleData(heroDetails: HeroDetails) {
        with(binding) {
            heroDetailsErrorView.root.hide()
            heroDetailsLoadingView.root.hide()
            heroDetailsContentContainer.show()
            heroDetailsNameTextView.text = heroDetails.name
            heroDetailsImageView.loadFromUrl(heroDetails.getPhoto(), requireContext())
        }
    }

    private fun showLoadingView() {
        with(binding) {
            heroDetailsErrorView.root.hide()
            heroDetailsContentContainer.hide()
            heroDetailsLoadingView.root.show()
        }
    }

    private fun showErrorView(error: Int) {
        with(binding) {
            heroDetailsLoadingView.root.hide()
            heroDetailsContentContainer.hide()
            heroDetailsErrorView.root.show()
            heroDetailsErrorView.errorMessage.setText(error)
        }
    }
}