package com.nicolaslera.marvel_cards.presentation.hero_details.error_handling

import com.nicolaslera.marvel_cards.R
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.core.error_handling.ErrorHandler

class HeroDetailsErrorHandler : ErrorHandler() {
    override fun getErrorMessage(code: AppError): Int {
        return when (code) {
            AppError.GET_HERO_DETAILS_ERROR -> R.string.hero_details_error
            else -> super.getErrorMessage(code)
        }
    }
}