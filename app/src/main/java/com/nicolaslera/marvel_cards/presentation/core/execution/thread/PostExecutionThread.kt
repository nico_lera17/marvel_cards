package com.nicolaslera.marvel_cards.presentation.core.execution.thread

import io.reactivex.rxjava3.core.Scheduler

interface PostExecutionThread {
    val scheduler: Scheduler
}