package com.nicolaslera.marvel_cards.presentation.hero_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.nicolaslera.marvel_cards.databinding.HeroListFragmentBinding
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItem
import com.nicolaslera.marvel_cards.domain.hero.model.HeroItemList
import com.nicolaslera.marvel_cards.presentation.core.MainActivity
import com.nicolaslera.marvel_cards.presentation.core.model.DataState
import com.nicolaslera.marvel_cards.presentation.core.view.hide
import com.nicolaslera.marvel_cards.presentation.core.view.show
import com.nicolaslera.marvel_cards.presentation.hero_list.adapter.HeroListAdapter
import com.nicolaslera.marvel_cards.presentation.hero_list.viewmodel.HeroListDataState
import com.nicolaslera.marvel_cards.presentation.hero_list.viewmodel.HeroListViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class HeroListFragment : Fragment(), HeroListAdapter.OnHeroSelectedListener {

    private val heroListViewModel: HeroListViewModel by viewModel()
    private var _binding: HeroListFragmentBinding? = null
    private val binding get() = _binding!!

    private val adapter: HeroListAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HeroListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupData()
    }

    private fun setupView() {
        adapter.onHeroSelectedListener = this
        binding.heroListRecyclerView.adapter = adapter
        binding.heroListErrorView.errorRetry.setOnClickListener {
            heroListViewModel.fetchHeroes()
        }
        binding.heroListLoadMoreButton.setOnClickListener {
            heroListViewModel.fetchHeroes()
        }
    }

    private fun setupData() {
        heroListViewModel.getHeroes().observe(viewLifecycleOwner, {
            handleDataState(it)
        })
    }

    private fun handleDataState(heroListState: HeroListDataState) {
        when (heroListState) {
            is DataState.Loading -> showLoadingView()
            is DataState.Success -> handleData(heroListState.data)
            is DataState.Error -> showErrorView(heroListState.error)
        }
    }

    private fun handleData(heroItemList: HeroItemList) {
        with(binding) {
            heroListLoadingView.root.hide()
            heroListErrorView.root.hide()
            heroListContentContainer.show()
        }
        with(adapter) {
            addHeroes(heroItemList.heroList)
            notifyDataSetChanged()
        }
        setLoadMoreButton(heroItemList)
    }

    private fun setLoadMoreButton(heroItemList: HeroItemList) {
        val nextPage = heroItemList.offset + HeroListViewModel.HERO_LIMIT
        if (heroItemList.totalItemCount in 0 until nextPage) {
            binding.heroListLoadMoreButton.hide()
        } else {
            binding.heroListLoadMoreButton.show()
        }
    }

    private fun showLoadingView() {
        with(binding) {
            heroListLoadingView.root.show()
            heroListContentContainer.hide()
            heroListErrorView.root.hide()
        }
    }

    private fun showErrorView(error: Int) {
        with(binding) {
            heroListLoadingView.root.hide()
            heroListContentContainer.hide()
            heroListErrorView.root.show()
            heroListErrorView.errorMessage.setText(error)
        }
    }

    override fun onHeroSelected(heroItem: HeroItem) {
        (activity as? MainActivity)?.navigateToHeroDetails(heroId = heroItem.id)
    }

    override fun onResume() {
        super.onResume()
        adapter.clear()
        heroListViewModel.onClear()
        heroListViewModel.fetchHeroes()
    }
}