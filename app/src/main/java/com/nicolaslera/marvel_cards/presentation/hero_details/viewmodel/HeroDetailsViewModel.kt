package com.nicolaslera.marvel_cards.presentation.hero_details.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nicolaslera.marvel_cards.domain.hero.model.HeroDetails
import com.nicolaslera.marvel_cards.domain.hero.use_case.GetHeroDetailsUseCase
import com.nicolaslera.marvel_cards.presentation.core.error_handling.AppError
import com.nicolaslera.marvel_cards.presentation.core.error_handling.ErrorHandler
import com.nicolaslera.marvel_cards.presentation.core.model.DataState
import com.nicolaslera.marvel_cards.presentation.core.view_model.BaseViewModel

typealias HeroDetailsDataState = DataState<HeroDetails>

class HeroDetailsViewModel(
    private val getHeroDetailsUseCase: GetHeroDetailsUseCase,
    private val errorHandler: ErrorHandler
) : BaseViewModel() {

    private val heroDetailsLiveData: MutableLiveData<HeroDetailsDataState> = MutableLiveData()

    fun getHeroDetails(): LiveData<HeroDetailsDataState> = heroDetailsLiveData

    fun fetchHeroDetails(id: Long) {
        heroDetailsLiveData.value = DataState.Loading()
        compositeDisposable.add(getHeroDetailsUseCase.execute(id).subscribe({
            heroDetailsLiveData.value = DataState.Success(it)
        }, {
            heroDetailsLiveData.value =
                DataState.Error(errorHandler.getErrorMessage(AppError.GET_HERO_DETAILS_ERROR))
        }))
    }
}