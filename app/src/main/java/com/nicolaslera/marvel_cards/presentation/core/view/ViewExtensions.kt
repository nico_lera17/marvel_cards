package com.nicolaslera.marvel_cards.presentation.core.view

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.nicolaslera.marvel_cards.R

fun View.show() {
    if (this.visibility != View.VISIBLE) {
        this.visibility = View.VISIBLE
    }
}

fun View.hide() {
    if (this.visibility == View.VISIBLE) {
        this.visibility = View.GONE
    }
}

fun ImageView.loadFromUrl(url: String, context: Context) {
    val uri = url.toUri().buildUpon().scheme("https").build()
    Glide.with(context)
        .load(uri)
        .placeholder(R.drawable.ic_error_placeholder)
        .into(this)
}