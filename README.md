# Marvel Cards

Este proyecto contiene el código de la aplicación que obtiene la lista de heroes de la API de marvel y los muestra por pantalla. Además, permite navegar a los detalles de cada héroe.

## Importante
Para poder utilizar la app de manera correcta deberá añadir sus claves de la api de marvel al final fichero gradle.properties del proyecto con el siguiente formato
```
MARVEL_PUBLIC_KEY="your_key"
MARVEL_PRIVATE_KEY="your_key"
```
### Arquitectura
Para este proyecto se ha hecho una adapatación propia del concepto de Clean Architecture, dividiendo el código en 4 módulos:
- Presentation: Contiene toda la lógica relacionada con la interfaz y el tratamiento de su estado. Hace uso de view binding.
- Domain: Contiene los use cases y la declaración del repositorio de datos
- Data: Contiene la implementación del repositorio y la lógica para diferenciar entre información cahceada y remota
- Datasources: Contiene el código tanto para acceder a la base de datos como para comunicarse con la API de Marvel

Además, se ha implementado una arquitectura MVVM en la capa de presentación. Para ello se ha utilizado live data.

### Bibliotecas
- Para trabajar con la información de manera asíncrona se ha hecho uso de RX Java y RX Kotlin v3
- Para almacenar la información de manera local se ha hecho uso de Room con su adaptación a rx
- Para comunicarse con la API de Marvel se ha hecho uso de Retrofit con su adaptación a rx
- Para mostrar las imágenes se ha hecho uso de Glide
- Para la inyección de dependencias se ha hecho uso de Koin
- Se han utilizado las bibliotecas de navegación de androidx para implementar la navegación

### Testing
Se han implementado test unitarios para comprobar el correcto funcionamiento del código a lo largo detoda la aplicación haciendo uso de JUnit y Mockito para kotlin.

Además, se han implementado test de espresso básicos para comprobar que los datos se mostraban de maneracorrecta y que los estados se trataban correctamente en los fragmentos.